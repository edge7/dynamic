# Here you can add all the business logic you want 
BUCKET_PIPE = """
build:
        stage: build
        before_script:
            - echo "hello, I am the bucket generator"
        script:
            - echo "bye"
"""

with open('bucket-child-gitlab-ci.yml', 'w+') as f:
    f.write(BUCKET_PIPE)

SERVICE_ACCOUNT_PIPE = """
build:
        stage: build
        before_script:
            - echo "hello, I am the service account generator"
        script:
            - echo "bye"
"""

with open('service-account-child-gitlab-ci.yml', 'w+') as f:
    f.write(BUCKET_PIPE)
    
